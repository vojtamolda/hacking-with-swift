// swift-tools-version:5.2
import PackageDescription


let package = Package(
    name: "DTO",
    products: [
        .library(name: "DTO", targets: ["DataTransferObject"]),
    ],
    dependencies: [ ],
    targets: [
	    .target(name: "DataTransferObject", dependencies: [ ]),
    ]
)
