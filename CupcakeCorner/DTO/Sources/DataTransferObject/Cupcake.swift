import Foundation


public struct Cupcake: Codable {
    public var id: UUID?
    public var name: String
    public var description: String
    public var price: Int
    
    public init(id: UUID? = nil, name: String, description: String, price: Int) {
        self.id = id
        self.name = name
        self.description = description
        self.price = price
    }
}
