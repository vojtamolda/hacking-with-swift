import Foundation


public struct Order: Codable {
    public var id: UUID?
    public var cupcakeIDs: [UUID]
    
    public struct Address: Codable {
        public var name: String
        public var street: String
        public var city: String
        public var zip: Int
        
        public init(name: String, street: String, city: String, zip: Int) {
            self.name = name
            self.street = street
            self.city = city
            self.zip = zip
        }
    }
    public var address: Address
    
    public var createdAt: Date?

    public init(id: UUID? = nil, cupcakeIDs: [UUID], address: Address, createdAt: Date? = nil) {
        self.id = id
        self.cupcakeIDs = cupcakeIDs
        self.address = address
        self.createdAt = createdAt
    }
}


public extension Order {
    var cost: Int {
        // TODO:
        return 0
    }
}


public extension Order.Address {
    var deliverable: Bool {
        if name.isEmpty || street.isEmpty || city.isEmpty || zip.digits == 6 {
            return false
        }

        return true
    }
}



fileprivate extension Int {
    var digits: Int {
        var (digits, remainder) = (0, self)
        repeat {
            (remainder, digits) = (remainder / 10, digits + 1)
        } while remainder > 0
        return digits
    }
}
