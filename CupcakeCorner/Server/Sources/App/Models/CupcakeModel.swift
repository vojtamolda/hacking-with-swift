import Vapor
import Fluent
import DataTransferObject


/// Allows cupcakes to be stored to and loaded from database.
final class CupcakeModel: Model {
    static let schema = "cupcakes"
    
    @ID(key: .id)
    var id: UUID?

    @Field(key: "name")
    var name: String
    
    @Field(key: "description")
    var description: String
    
    @Field(key: "price")
    var price: Int

    /// An empty initializer called internally by *Fluent* to initialize models returned by queries.
    init() { }
}

extension CupcakeModel {
    /// Creates cupcake database model from its DTO.
    convenience init(from dto: Cupcake) {
        self.init()
        id = dto.id
        name = dto.name
        description = dto.description
        price = dto.price
    }
}

extension Cupcake {
    /// Creates cupcake DTO from its database model.
    init(from model: CupcakeModel) {
        self.init(id: model.id, name: model.name, description: model.description,
                  price: model.price)
    }
}

/// Allows cupcake DTO to be encoded to and decoded directly from HTTP messages.
extension Cupcake: Content { }
