import Vapor
import Fluent
import DataTransferObject


/// Allows orders and their items to be stored to and loaded from database.
final class OrderModel: Model {
    static let schema = "orders"
    
    @ID(key: .id)
    var id: UUID?
    
    final class Address: Fields {
        @Field(key: "name")
        var name: String

        @Field(key: "street")
        var street: String
        
        @Field(key: "city")
        var city: String
        
        @Field(key: "zip")
        var zip: Int
        
        init() { }
    }
    @Group(key: "address")
    var address: OrderModel.Address
    
    @Timestamp(key: "created_at", on: .create)
    var createdAt: Date?

    final class Item: Model {
        static var schema = "orders_items"
        
        @ID(key: .id)
        var id: UUID?

        @Parent(key: "order_id")
        var order: OrderModel
 
        @Parent(key: "cupcake_id")
        var cupcake: CupcakeModel

        /// An empty initializer called internally by Fluent to initialize models returned by queries.
        init() { }
        
    }
    @Siblings(through: OrderModel.Item.self, from: \.$order, to: \.$cupcake)
    var cupcakes: [CupcakeModel]

    /// An empty initializer called internally by *Fluent* to initialize models returned by queries.
    init() { }
}

extension OrderModel {
    /// Creates order database model from its DTO.
    convenience init(from dto: Order) {
        self.init()
        id = dto.id
        address = OrderModel.Address(from: dto.address)
        createdAt = dto.createdAt
    }
}

extension OrderModel.Address {
    /// Creates order address database model from its DTO.
    convenience init(from dto: Order.Address) {
        self.init()
        name = dto.name
        street = dto.street
        city = dto.city
        zip = dto.zip
    }
}

extension Order {
    /// Creates order DTO from its database model.
    init(from model: OrderModel) {
        self.init(id: model.id, cupcakeIDs: model.cupcakes.compactMap { $0.id },
                  address: Address(from: model.address), createdAt: model.createdAt)
    }
}

extension Order.Address {
    /// Creates order address DTO from its database model.
    init(from model: OrderModel.Address) {
        self.init(name: model.name, street: model.street, city: model.city, zip: model.zip)
    }
}

/// Allows order DTO to be encoded to and decoded directly from HTTP messages.
extension Order: Content { }
