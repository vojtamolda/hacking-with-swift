import Vapor
import Foundation
import DataTransferObject


/// Controls basic CRUD operations on cupcakes.
struct CupcakeController: RouteCollection {
    static private let route: PathComponent = "cupcake"
    static private let thumbnailDir = URL(fileURLWithPath: "Public/Cupcake", isDirectory: true)

    /// Registers collection of routes managed by the the controller.
    func boot(routes: RoutesBuilder) throws {
        let cupcakeRoutes = routes.grouped(Self.route)

        cupcakeRoutes.get(use: index)
        cupcakeRoutes.post(use: create)
        cupcakeRoutes.group(":cupcakeID") { req in
            req.delete(use: delete)
        }
    }

    /// Returns a list of all cupcakes from the database.
    func index(_ req: Request) throws -> EventLoopFuture<[Cupcake]> {
        let cupcakes = CupcakeModel.query(on: req.db).all()
        return cupcakes.mapEach { Cupcake(from: $0) }
    }

    private struct Thumbnail: Content {
        var thumbnail: Data
    }

    /// Stores a new cupcake to the database.
    func create(_ req: Request) throws -> EventLoopFuture<Cupcake> {
        let dto = try req.content.decode(Cupcake.self)
        let cupcake = CupcakeModel(from: dto)
        let savedCupcake = cupcake.save(on: req.db)

        let savedThumbnail = savedCupcake.flatMapThrowing {
            let thumbnail = try req.content.decode(Thumbnail.self)
            let file = try Self.thumbnailDir.appendingPathComponent("\(cupcake.requireID()).jpg")
            try thumbnail.thumbnail.write(to: file)
        }

        return savedThumbnail.transform(to: Cupcake(from: cupcake))
    }
    
    /// Drops a parameterized cupcake from the database.
    func delete(req: Request) throws -> EventLoopFuture<HTTPStatus> {
        return CupcakeModel.find(req.parameters.get("cupcakeID"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { $0.delete(on: req.db) }
            .transform(to: .ok)
    }
}
