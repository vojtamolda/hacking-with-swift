import Vapor
import Fluent
import DataTransferObject


/// Controls basic CRUD operations on orders.
struct OrderController: RouteCollection {
    static private let route: PathComponent = "order"

    /// Registers collection of routes managed by the the controller.
    func boot(routes: RoutesBuilder) throws {
        let orderRoutes = routes.grouped(Self.route)

        orderRoutes.get(use: index)
        orderRoutes.post(use: create)
   }
    
    /// Returns a list of all orders with the bought cupcakes from the database.
    func index(_ req: Request) throws -> EventLoopFuture<[Order]> {
        let orders = OrderModel.query(on: req.db).with(\.$cupcakes).all()
        return orders.mapEach { Order(from: $0) }
    }

    /// Saves a new order to the database and attaches the bought cupcakes through a sibling relationship.
    func create(_ req: Request) throws -> EventLoopFuture<Order> {
        let dto = try req.content.decode(Order.self)
        let order = OrderModel(from: dto)

        let orderSaved = order.save(on: req.db)
        let retrievedCupcakes = CupcakeModel.query(on: req.db)
            .filter(\.$id ~~ dto.cupcakeIDs)
            .all()
            .map { cupcakes -> [UUID: CupcakeModel] in
                let ids = cupcakes.map { $0.id! }
                return Dictionary(uniqueKeysWithValues: zip(ids, cupcakes))
            }
 
        let cupcakesAttached = orderSaved.and(retrievedCupcakes).flatMap {
            (_, retrievedCupcakes) -> EventLoopFuture<Void> in
            
            let everyOrderedCupcake = dto.cupcakeIDs.map { id -> CupcakeModel in
                guard let orderedCupcake = retrievedCupcakes[id] else {
                    fatalError("Non-existent cupcake with id \(id) in order.")
                }
                return orderedCupcake
            }
            return order.$cupcakes.attach(everyOrderedCupcake, on: req.db)
        }.flatMap {
            order.$cupcakes.load(on: req.db)
        }
        
        return cupcakesAttached.transform(to: Order(from: order))
    }
}
