import Vapor


struct HomeContent: Content {
    let cupcakes: [CupcakeModel]
    let orders: [OrderModel]
}
