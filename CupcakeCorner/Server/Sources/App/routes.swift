import Vapor
import Fluent
import Leaf


/// Register your application's routes here.
func routes(_ app: Application) throws {
    // Home page
    app.get { req -> EventLoopFuture<View> in
        let cupcakes = CupcakeModel.query(on: req.db).all()
        let orders = OrderModel.query(on: req.db).with(\.$cupcakes).all()
        
        let homeView = cupcakes.and(orders).flatMap { (cupcakes, orders) -> EventLoopFuture<View> in
            let homeContent = HomeContent(cupcakes: cupcakes, orders: orders)
            return req.view.render("Home", homeContent)
        }
        return homeView
    }

    // Register API end-point controllers
    try app.register(collection: CupcakeController())
    try app.register(collection: OrderController())
}
