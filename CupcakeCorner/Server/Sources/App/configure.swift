import Vapor
import FluentSQLiteDriver
import Leaf


/// Called before your application initializes to configure it.
public func configure(_ app: Application) throws {
    // Serve files from /Public folder
    app.middleware.use(FileMiddleware(publicDirectory: app.directory.publicDirectory))

    // Leaf templating framework
    app.views.use(.leaf)

    // SQLite database
    app.databases.use(.sqlite(.file("CupcakeCorner.db")), as: .sqlite)

    // Database migrations
    app.migrations.add(CupcakeMigration())
    app.migrations.add(OrderMigration())
    app.migrations.add(OrderMigration.ItemMigration())

    // Register routes to the router
    app.routes.defaultMaxBodySize = "2 mb"
    try routes(app)
}
