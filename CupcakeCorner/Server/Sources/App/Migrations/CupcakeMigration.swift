import Fluent


/// Allows dynamic migration of `CupcakeModel` database schema.
struct CupcakeMigration: Migration {

    /// Creates the schema for storage of cupcakes.
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        let cupcakeSchema = database.schema(CupcakeModel.schema)
            .id()
            .field("name", .string, .required)
            .field("description", .string, .required)
            .field("price", .uint, .required)
        return cupcakeSchema.create()
    }

    /// Roll backs the schema for storage of cupcakes.
    func revert(on database: Database) -> EventLoopFuture<Void> {
        let cupcakeSchema = database.schema(CupcakeModel.schema)
        return cupcakeSchema.delete()
    }
}
