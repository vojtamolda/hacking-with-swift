import Fluent


/// Allows dynamic migration of `OrderModel` database schema.
struct OrderMigration: Migration {

    /// Allows dynamic migration of `OrderModel.ItemModel` database schema.
    struct ItemMigration: Migration {
        
        /// Creates the schema for storage of order items.
        func prepare(on database: Database) -> EventLoopFuture<Void> {
            let orderItemSchema = database.schema(OrderModel.Item.schema)
                .id()
                .field("order_id", .uuid, .required, .references(OrderModel.schema, .id))
                .field("cupcake_id", .uuid, .required, .references(CupcakeModel.schema, .id))
            return orderItemSchema.create()
        }
     
        /// Roll backs the schema for storage of order items.
        func revert(on database: Database) -> EventLoopFuture<Void> {
            let orderItemSchema = database.schema(OrderModel.schema)
            return orderItemSchema.delete()
            
        }

    }

    /// Creates the schema for storage of  orders.
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        let orderSchema = database.schema(OrderModel.schema)
            .id()
            .field("address_name", .string, .required)
            .field("address_street", .string, .required)
            .field("address_city", .string, .required)
            .field("address_zip", .int, .required)
            .field("created_at", .datetime, .required)
        return orderSchema.create()
    }

    /// Roll backs the schema for storage of orders.
    func revert(on database: Database) -> EventLoopFuture<Void> {
        let orderSchema = database.schema(OrderModel.schema)
        return orderSchema.delete()
    }
}
