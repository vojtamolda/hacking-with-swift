import SwiftUI
import DataTransferObject


struct CupcakeRowView: View {
    @State var cupcake: CupcakeViewModel

    var action: (() -> Void)? = nil

    struct Thumbnail: View {
        @Binding var cupcake: CupcakeViewModel
        @Environment(\.imageCache) var cache: AsyncImage.Cache

        var body: some View {
            AsyncImage(contentsOf: cupcake.thumbnail,
                       cache: cache,
                       placeholder: ProgressView()
                            .progressViewStyle(CircularProgressViewStyle()),
                       configuration: { $0.resizable() })
                .scaledToFill()
                .frame(width: 48, height: 48)
                .clipShape(RoundedRectangle(cornerRadius: 5))
        }
    }

    struct Content: View {
        @Binding var cupcake: CupcakeViewModel

        var body: some View {
            HStack {
                Thumbnail(cupcake: $cupcake)
                VStack(alignment: .leading) {
                    Text(self.cupcake.name)
                        .font(.headline)
                    Text(self.cupcake.description)
                        .font(.caption)
                    Text("$\(self.cupcake.price)")
                        .font(.caption2)
                }
            }
        }
    }

    var body: some View {
        if let action = self.action {
            Button(action: action) {
                Content(cupcake: $cupcake)
            }
        } else {
            Content(cupcake: $cupcake)
        }
    }
}

// MARK: - Preview

struct CupcakeRowView_Previews: PreviewProvider {
    static var previews: some View {
        let dummy = CupcakeViewModel(name: "Dummy", description: "Description text", price: 5)
        return CupcakeRowView(cupcake: dummy)
    }
}
