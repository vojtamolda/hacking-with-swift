import SwiftUI
import DataTransferObject


struct OrderView: View {
    @ObservedObject var order = OrderViewModel(
        cupcakes: [],
        address: .init(name: "", street: "", city: "", zip: 0)
    )
    @ObservedObject var submitter = OrderViewModel.Submitter()

    struct OrderForm: View {
        @ObservedObject var order: OrderViewModel
        @ObservedObject var submitter: OrderViewModel.Submitter

        @State private var showingPickCupcake = false

        var body: some View {
            Form {
                Section(header: Text("Cupcakes")) {
                    ForEach(order.cupcakes) { cupcake in
                        CupcakeRowView(cupcake: cupcake)
                    }
                    .onDelete { offsets in
                        withAnimation { order.cupcakes.remove(atOffsets: offsets) }
                    }

                    Button("Add") {
                        withAnimation { showingPickCupcake = true }
                    }
                }

                Section(header: Text("Address")) {
                    TextField("Name", text: $order.address.name)
                    TextField("Street", text: $order.address.street)
                    TextField("City", text: $order.address.city)
                    TextField("Zip", text: Binding(
                        get: { order.address.zip != 0 ? String(order.address.zip) : "" },
                        set: { order.address.zip = Int($0) ?? 0 } ))
                    .keyboardType(.numberPad)
                }

                Button(action: {
                    submitter.order = self.order
                    withAnimation { submitter.submit() }
                    print("Order tapped!")
                }) {
                    HStack {
                        Image(systemName: "cart")
                        Text("Order")
                    }
                    .font(.headline)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding()
                }
                .disabled(!order.address.deliverable || order.cupcakes.isEmpty)
            }
            
            .sheet(isPresented: $showingPickCupcake) {
                CupcakePickerView(order: order)
            }

        }
    }
    
    struct OrderSubmittingProgress: View {
        var body: some View {
            RoundedRectangle(cornerRadius: 25)
                .frame(width: 120, height: 100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .foregroundColor(.secondary)
                .shadow(radius: 20)
            ProgressView {
                Text("Ordering")
                    .foregroundColor(.primary)
            }
        }
    }
    
    var body: some View {
        ZStack {
            NavigationView {
                OrderForm(order: order, submitter: submitter)
                .navigationBarTitle("CupcakeCorner")
            }
            .disabled(submitter.isSubmitting)
            .blur(radius: submitter.isSubmitting ? 10 : 0)
            
            if submitter.isSubmitting {
                OrderSubmittingProgress()
            }
        }
    }
}

// MARK: - Preview

struct OrderView_Previews: PreviewProvider {
    static var previews: some View {
        let order = OrderViewModel(
            cupcakes: [
                CupcakeViewModel(name: "Vanilla", description: "Classic cupcake", price: 5),
                CupcakeViewModel(name: "Strawberry", description: "Sweet one", price: 6),
            ],
            address: .init(name: "", street: "", city: "", zip: 0))

        let submitter = OrderViewModel.Submitter()
        submitter.isSubmitting = true

        return Group {
            OrderView(order: order)
            OrderView(order: order, submitter: submitter)
        }
    }
}
