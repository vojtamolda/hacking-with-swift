import SwiftUI
import DataTransferObject


struct CupcakePickerView: View {
    @Environment(\.presentationMode) var presentationMode

    @ObservedObject var order = OrderViewModel(
        cupcakes: [],
        address: .init(name: "", street: "", city: "", zip: 0)
    )

    @ObservedObject private var loader = CupcakeViewModel.Loader()

    var body: some View {
        NavigationView {
            List(loader.cupcakes) { cupcake in
                CupcakeRowView(cupcake: cupcake) {
                    self.order.cupcakes.append(cupcake)
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
            
            .navigationTitle("Add Cupcake")
            
            .toolbar {
                ToolbarItem {
                    Button("Cancel") { self.presentationMode.wrappedValue.dismiss() }
                }
            }
        }
        .onAppear { loader.load() }
        .onDisappear { loader.cancel() }
    }
}

// MARK: - Preview

struct CupcakePickerView_Previews: PreviewProvider {
    static var previews: some View {
        return CupcakePickerView()
    }
}
