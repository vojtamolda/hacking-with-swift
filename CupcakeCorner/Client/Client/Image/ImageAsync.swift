import SwiftUI
import Combine


struct AsyncImage: View {
    @ObservedObject private var loader: Loader

    private let placeholder: AnyView?
    private let configuration: (Image) -> Image
    
    init<V: View>(url: URL, cache: Cache? = nil, placeholder: V? = nil,
                  configuration: @escaping (Image) -> Image = { $0 }) {
        loader = Loader(url: url, cache: cache)
        self.placeholder = AnyView(placeholder)
        self.configuration = configuration
    }
    
    var body: some View {
        image
            .onAppear(perform: loader.load)
            .onDisappear(perform: loader.cancel)
    }
    
    private var image: some View {
        Group {
            if loader.image != nil {
                configuration(Image(uiImage: loader.image!))
            } else {
                placeholder
            }
        }
    }
}

// MARK: - Loader

extension AsyncImage {
    class Loader: ObservableObject {
        @Published var image: UIImage?
        
        private(set) var isLoading = false
        
        private let url: URL
        private var cache: Cache?
        private var cancellable: AnyCancellable?
        
        private static let imageProcessingQueue = DispatchQueue(label: "image-processing")
        
        init(url: URL, cache: Cache? = nil) {
            self.url = url
            self.cache = cache
        }
        
        deinit {
            cancellable?.cancel()
        }
        
        func load() {
            guard !isLoading else { return }

            if let image = cache?[url] {
                self.image = image
                return
            }
            
            cancellable = URLSession.shared.dataTaskPublisher(for: url)
                .map { UIImage(data: $0.data) }
                .replaceError(with: nil)
                .handleEvents(receiveSubscription: { [weak self] _ in self?.onStart() },
                              receiveOutput: { [weak self] in self?.cache($0) },
                              receiveCompletion: { [weak self] _ in self?.onFinish() },
                              receiveCancel: { [weak self] in self?.onFinish() })
                .subscribe(on: Self.imageProcessingQueue)
                .receive(on: DispatchQueue.main)
                .assign(to: \.image, on: self)
        }
        
        func cancel() {
            cancellable?.cancel()
        }
        
        private func onStart() {
            isLoading = true
        }
        
        private func onFinish() {
            isLoading = false
        }
        
        private func cache(_ image: UIImage?) {
            image.map { cache?[url] = $0 }
        }
    }
}

// MARK: - Cache

extension AsyncImage {
    struct Cache {
        struct Key: EnvironmentKey {
            static let defaultValue: AsyncImage.Cache = AsyncImage.Cache()
        }

        private let cache = NSCache<NSURL, UIImage>()
        
        subscript(_ key: URL) -> UIImage? {
            get { cache.object(forKey: key as NSURL) }
            set { newValue == nil ? cache.removeObject(forKey: key as NSURL) : cache.setObject(newValue!, forKey: key as NSURL) }
        }
    }
}

// MARK: - Environment

extension EnvironmentValues {
    var imageCache: AsyncImage.Cache {
        get { self[AsyncImage.Cache.Key.self] }
        set { self[AsyncImage.Cache.Key.self] = newValue }
    }
}
