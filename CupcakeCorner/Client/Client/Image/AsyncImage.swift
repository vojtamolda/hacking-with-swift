import SwiftUI
import Combine


/// Image asynchronously loaded from a URL that displays a placeholder during download.
struct AsyncImage: View {
    @ObservedObject private var loader: Loader

    private let placeholder: AnyView?
    private let configuration: (Image) -> Image
    
    init<V: View>(contentsOf url: URL, cache: Cache? = nil, placeholder: V? = nil,
                  configuration: @escaping (Image) -> Image = { $0 }) {
        self.placeholder = AnyView(placeholder)
        self.configuration = configuration
        
        loader = Loader(from: url, cache: cache)
    }
    
    var body: some View {
        Group {
            if loader.image != nil {
                configuration(Image(uiImage: loader.image!))
            } else {
                placeholder
            }
        }
        .onAppear(perform: loader.load)
        .onDisappear(perform: loader.cancel)
    }
}

// MARK: - Loader

extension AsyncImage {
    /// Asynchronous loader of images.
    class Loader: ObservableObject {
        @Published var image: UIImage?

        private(set) var isLoading = false
        
        private let url: URL
        private var cache: Cache?
        private var task: AnyCancellable?
        
        private static let queue = DispatchQueue(label: "AsyncImage.Loader")
        
        init(from url: URL, cache: Cache? = nil) {
            self.url = url
            self.cache = cache
        }
        
        deinit {
            task?.cancel()
        }
        
        func load() {
            guard !isLoading else { return }

            if let image = cache?[url] {
                self.image = image
                return
            }
            
            task = URLSession.shared.dataTaskPublisher(for: url)
                .map { (data, response) -> UIImage? in UIImage(data: data) }
                .replaceError(with: nil)
                .handleEvents(receiveSubscription: { [weak self] _ in self?.start() },
                              receiveOutput: { [weak self] in self?.cache($0) },
                              receiveCompletion: { [weak self] _ in self?.complete() },
                              receiveCancel: { [weak self] in self?.complete() })
                .subscribe(on: Self.queue)
                .receive(on: DispatchQueue.main)
                .assign(to: \.image, on: self)
        }

        func cancel() {
            task?.cancel()
        }
        
        private func start() {
            isLoading = true
        }

        private func cache(_ image: UIImage?) {
            cache?[url] = image
        }
        
        private func complete() {
            isLoading = false
        }
    }
}

// MARK: - Cache

extension AsyncImage {
    /// Temporary storage for loaded images keyed by their URLs.
    struct Cache {
        struct Key: EnvironmentKey {
            static let defaultValue: AsyncImage.Cache = AsyncImage.Cache()
        }

        private let cache = NSCache<NSURL, UIImage>()
        
        /// Returns corresponding stored image or nil for non existing URLs.
        subscript(_ key: URL) -> UIImage? {
            get { cache.object(forKey: key as NSURL) }
            set {
                if newValue == nil {
                    cache.removeObject(forKey: key as NSURL)
                } else {
                    cache.setObject(newValue!, forKey: key as NSURL)
                }
            }
        }
    }
}

extension EnvironmentValues {
    /// Temporary storage for loaded images keyed by their URLs.
    var imageCache: AsyncImage.Cache {
        get { self[AsyncImage.Cache.Key.self] }
        set { self[AsyncImage.Cache.Key.self] = newValue }
    }
}
