import Foundation
import Combine
import DataTransferObject


final class CupcakeViewModel: Identifiable {
    @Published var id: UUID?
    @Published var name: String
    @Published var description: String
    @Published var price: Int

    var thumbnail: URL {
        Self.apiEndpoint.appendingPathComponent("\(id ?? UUID(uuidString: "Empty")!).jpg")
    }

    private static let apiEndpoint = URL(string: "http://localhost:8080/cupcake")!
    
    init(id: UUID? = nil, name: String, description: String, price: Int) {
        self.id = id
        self.name = name
        self.description = description
        self.price = price
    }
}

extension CupcakeViewModel {
    convenience init(from cupcake: Cupcake) {
        self.init(id: cupcake.id,
                  name: cupcake.name,
                  description: cupcake.description,
                  price: cupcake.price)
    }
}

// MARK: - Loader

extension CupcakeViewModel {
    /// Asynchronous loader of the cupcake list.
    class Loader: ObservableObject {
        @Published var cupcakes: [CupcakeViewModel] = []

        private(set) var isLoading = false
        
        private let url: URL
        private var task: AnyCancellable?
        
        private static let queue = DispatchQueue(label: "CupcakeViewModel.Loader")
        
        init(from url: URL = CupcakeViewModel.apiEndpoint) {
            self.url = url
        }
        
        deinit {
            task?.cancel()
        }
        
        func load() {
            guard !isLoading else { return }
            
            var request = URLRequest(url: CupcakeViewModel.apiEndpoint)
            request.httpMethod = "GET"
            
            task = URLSession.shared.dataTaskPublisher(for: request)
                .map { (data, response) in data }
                .decode(type: [Cupcake].self, decoder: JSONDecoder())
                .map { $0.map { CupcakeViewModel(from: $0) } }
                .replaceError(with: [])
                .subscribe(on: Self.queue)
                .receive(on: DispatchQueue.main)
                .handleEvents(receiveSubscription: { [weak self] _ in self?.start() },
                              receiveCompletion: { [weak self] _ in self?.complete() },
                              receiveCancel: { [weak self] in self?.complete() })
                .assign(to: \.cupcakes, on: self)
        }
        
        func cancel() {
            task?.cancel()
        }
        
        private func start() {
            isLoading = true
        }

        private func complete() {
            isLoading = false
        }
    }
}
