import Foundation
import Combine
import DataTransferObject


final class OrderViewModel: ObservableObject {
    var id: UUID? = nil
    @Published public var cupcakes: [CupcakeViewModel]
    @Published var address: Address
    @Published var createdAt: Date?

    private static let apiEndpoint = URL(string: "http://localhost:8080/order")!
    
    init(id: UUID? = nil, cupcakes: [CupcakeViewModel], address: Address, createdAt: Date? = nil) {
        self.id = id
        self.cupcakes = cupcakes
        self.address = address
        self.createdAt = createdAt
    }
}

extension Order {
    init(from model: OrderViewModel) {
        self.init(id: model.id, cupcakeIDs: model.cupcakes.compactMap { $0.id },
                  address: Address(from: model.address), createdAt: model.createdAt)
    }
}

// MARK: - Address

extension OrderViewModel {
    final class Address: ObservableObject {
        @Published var name: String
        @Published var street: String
        @Published var city: String
        @Published var zip: Int
        
        var deliverable: Bool {
            let address = Order.Address(from: self)
            return address.deliverable
        }

        init(name: String, street: String, city: String, zip: Int) {
            self.name = name
            self.street = street
            self.city = city
            self.zip = zip
        }
        
        convenience init(from address: Order.Address) {
            self.init(name: address.name, street: address.street,
                      city: address.city, zip: address.zip)
        }
    }
}

extension Order.Address {
    init(from model: OrderViewModel.Address) {
        self.init(name: model.name, street: model.street, city: model.city, zip: model.zip)
    }
}

// MARK: - Submitter

extension OrderViewModel {
    /// Asynchronous order submitter.    
    class Submitter: ObservableObject {
        var order: OrderViewModel?
        @Published var isSubmitting = false
        
        private let url = OrderViewModel.apiEndpoint
        private var task: AnyCancellable?
        
        private static let queue = DispatchQueue(label: "OrderViewModel.Submitter")

        init() { }
        
        deinit {
            task?.cancel()
        }
        
        func submit() {
            guard !isSubmitting else { return }
            guard order != nil else { return }
            isSubmitting = true

            var request = URLRequest(url: self.url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = try? JSONEncoder().encode(Order(from: order!))
            
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            
            task = URLSession.shared.dataTaskPublisher(for: request)
                .map { (data, response) in data }
                .decode(type: Order.self, decoder: decoder)
                .map { decodedOrder -> Bool in
                    self.order?.id = decodedOrder.id
                    print(decodedOrder)
                    print("Wait..."); sleep(5); print("Done!")
                    return false
                }
                .replaceError(with: false)
                .subscribe(on: Self.queue)
                .receive(on: DispatchQueue.main)
                .assign(to: \.isSubmitting, on: self)
        }
    }
}
