//
//  Astronaut.swift
//  Moonshot
//
//  Created by Vojta Molda on 7/3/20.
//

import Foundation

struct Astronaut: Codable, Identifiable {
    let id: String
    let name: String
    let description: String
}
