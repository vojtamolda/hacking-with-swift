//
//  MoonshotApp.swift
//  Moonshot
//
//  Created by Vojta Molda on 7/3/20.
//

import SwiftUI

@main
struct MoonshotApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
