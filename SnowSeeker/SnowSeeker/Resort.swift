//
//  Resort.swift
//  SnowSeeker
//
//  Created by Vojta Molda on 8/4/20.
//

import Foundation


struct Resort: Identifiable, Codable {
    let id: String
    let name: String
    let country: String
    let description: String
    let imageCredit: String
    let price: Int
    let size: Int
    let snowDepth: Int
    let elevation: Int
    let runs: Int
    let facilities: [String]
    
    var facilityTypes: [Facility] { facilities.map(Facility.init) }
    
    private static let allResorts: [Resort] = Bundle.main.decode("resorts.json")
    static let example = allResorts.randomElement()!
}
