//
//  SnowSeekerApp.swift
//  SnowSeeker
//
//  Created by Vojta Molda on 8/4/20.
//

import SwiftUI

@main
struct SnowSeekerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
