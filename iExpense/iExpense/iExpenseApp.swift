//
//  iExpenseApp.swift
//  iExpense
//
//  Created by Vojta Molda on 7/3/20.
//

import SwiftUI

@main
struct iExpenseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
