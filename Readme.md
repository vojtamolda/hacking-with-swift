
# Hacking with Swift


![Tunnel Graphic](https://www.hackingwithswift.com/img/bg-updates.jpg)

Hacking with Swift (SwiftUI edition) is a complete tutorial series written by [Paul Hudson](https://twitter.com/twostraws) for Swift 5.3 and iOS 14, taking from you beginner to advanced using hands-on projects.
- Website: [Hacking with Swift](https://www.hackingwithswift.com/books/ios-swiftui)
- Videos: [YouTube](https://www.youtube.com/playlist?list=PLuoeXyslFTuZRi4q4VT6lZKxYbr7so1Mr)


## 1. [`WeSplit/`](WeSplit/WeSplit/)
Learn the basics of SwiftUI with your first project.

<img src="WeSplit/Screenshot 1.png" width="30%">
<img src="WeSplit/Screenshot 2.png" width="30%">


## 2. [`GuessTheFlag/`](GuessTheFlag/GuessTheFlag/)
Build a game with stacks, images, and alerts.

<img src="GuessTheFlag/Screenshot 1.png" width="30%">
<img src="GuessTheFlag/Screenshot 2.png" width="30%">


## 3. Views and Modifiers
Dive deep into Swift's rendering system (no project).


## 4. [`BetterRest/`](BetterRest/BetterRest/)
Use machine learning to improve sleep.

<img src="BetterRest/Screenshot 1.png" width="30%">
<img src="BetterRest/Screenshot 2.png" width="30%">


## 5. [`WordScramble/`](WordScramble/WordScramble/)
Build a letter rearranging game with `List`.

<img src="WordScramble/Screenshot 1.png" width="30%">
<img src="WordScramble/Screenshot 2.png" width="30%">


## 6. [`Animation/`](Animation/Animation/)
Spruce up your UI with springs, bounces, and more.

<img src="Animation/Screenshot 1.png" width="30%">
<img src="Animation/Screenshot 2.png" width="30%">


## 7. [`iExpense/`](iExpense/iExpense/)
Bring in a second view with this expense tracking app.

<img src="iExpense/Screenshot 1.png" width="30%">
<img src="iExpense/Screenshot 2.png" width="30%">


## 8. [`Moonshot/`](Moonshot/Moonshot/)
Learn about space history with scroll views, Codable, and more.

<img src="Moonshot/Screenshot 1.png" width="30%">
<img src="Moonshot/Screenshot 2.png" width="30%">
<img src="Moonshot/Screenshot 3.png" width="30%">


## 9. [`Drawing/`](Drawing/Drawing/)
Use shapes, paths, colors, and more to create custom art for your app.

<img src="Drawing/Screenshot 1.png" width="30%">
<img src="Drawing/Screenshot 2.png" width="30%">


## 10. [`CupcakeCorner/`](CupcakeCorner/)
Build an app and server that sends and receives JSON cupcake orders.

<img src="CupcakeCorner/Client/Screenshot 1.png" width="30%">
<img src="CupcakeCorner/Client/Screenshot 2.png" width="30%">
<img src="CupcakeCorner/Client/Screenshot 3.png" width="30%">
<br>
<img src="CupcakeCorner/Server/Server.png" width="60%">


## 11. [`Bookworm/`](Bookworm/Bookworm/)
Use Core Data to build an app that tracks books you like.

<img src="Bookworm/Screenshot 1.png" width="30%">
<img src="Bookworm/Screenshot 2.png" width="30%">
<img src="Bookworm/Screenshot 3.png" width="30%">


## 12. Core Data
Take an in-depth tour of how SwiftUI and Core Data work together (no project).


## 13. [`Instafilter/`](Instafilter/Instafilter/)
Learn to link SwiftUI, UIKit, and Core Image in one app.

<img src="Instafilter/Screenshot 1.png" width="30%">
<img src="Instafilter/Screenshot 2.png" width="30%">
<img src="Instafilter/Screenshot 3.png" width="30%">


## 14. [`BucketList/`](BucketList/BucketList/)
Embed maps and make network calls in this life goals app.

<img src="BucketList/Screenshot 1.png" width="30%">
<img src="BucketList/Screenshot 2.png" width="30%">
<img src="BucketList/Screenshot 3.png" width="30%">


## 15.  [`Accessibility/`](Accessibility/Accessibility/)
Learn how to make your apps available to everyone.

<img src="Accessibility/Screenshot 1.png" width="30%">


## 16.  [`HotProspects/`](HotProspects/HotProspects/)
Build an app for conferences with tabs, context menus, and more.

<img src="HotProspects/Screenshot 1.png" width="30%">
<img src="HotProspects/Screenshot 2.png" width="30%">
<img src="HotProspects/Screenshot 3.png" width="30%">


## 17.  [`Flashzilla/`](Flashzilla/Flashzilla/)
Use gestures and haptics to build a learning app.

<img src="Flashzilla/Screenshot 1.png" width="45%">
<img src="Flashzilla/Screenshot 2.png" width="45%">


## 18. Layout And Geometry
Explore the inner workings of SwiftUI's layout system (no project).


## 19.  [`SnowSeeker/`](SnowSeeker/SnowSeeker/)
Build an app for ski resorts that works great on iPad.

<img src="SnowSeeker/Screenshot 1.png" width="30%">
<img src="SnowSeeker/Screenshot 2.png" width="30%">

---

Licensed under the MIT license. See [the text](License.txt) for more details.
