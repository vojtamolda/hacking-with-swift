//
//  InstafilterApp.swift
//  Instafilter
//
//  Created by Vojta Molda on 7/27/20.
//

import SwiftUI

@main
struct InstafilterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
