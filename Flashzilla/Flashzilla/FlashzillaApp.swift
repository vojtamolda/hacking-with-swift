//
//  FlashzillaApp.swift
//  Flashzilla
//
//  Created by Vojta Molda on 7/30/20.
//

import SwiftUI

@main
struct FlashzillaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
