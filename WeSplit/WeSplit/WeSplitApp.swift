//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Vojta Molda on 6/19/20.
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
