//
//  MKPointAnnotation+ObservableObject.swift
//  BucketList
//
//  Created by Vojta Molda on 7/29/20.
//

import MapKit

extension MKPointAnnotation: ObservableObject {
    public var wrappedTitle: String {
        get { self.title ?? "Unknown title" }
        set { title = newValue }
    }
    
    public var wrappedSubtitle: String {
        get { self.subtitle ?? "Unknown subtitle" }
        set { subtitle = newValue }
    }
}

extension MKPointAnnotation  {
    static var example: MKPointAnnotation {
        let annotation = MKPointAnnotation()
        annotation.title = "London"
        annotation.subtitle = "Home of the 2012 Summer Olympics"
        annotation.coordinate = CLLocationCoordinate2D(latitude: 51.3, longitude: -0.13)
        return annotation
    }
}
