//
//  BucketListApp.swift
//  BucketList
//
//  Created by Vojta Molda on 7/27/20.
//

import SwiftUI

@main
struct BucketListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
