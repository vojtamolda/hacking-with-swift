//
//  WordScrambleApp.swift
//  WordScramble
//
//  Created by Vojta Molda on 7/2/20.
//

import SwiftUI

@main
struct WordScrambleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
