//
//  AnimationApp.swift
//  Animation
//
//  Created by Vojta Molda on 7/3/20.
//

import SwiftUI

@main
struct AnimationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
