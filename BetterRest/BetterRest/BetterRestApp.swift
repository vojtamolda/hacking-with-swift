//
//  BetterRestApp.swift
//  BetterRest
//
//  Created by Vojta Molda on 7/2/20.
//

import SwiftUI

@main
struct BetterRestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
