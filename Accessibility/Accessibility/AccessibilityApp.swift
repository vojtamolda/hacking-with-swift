//
//  AccessibilityApp.swift
//  Accessibility
//
//  Created by Vojta Molda on 7/30/20.
//

import SwiftUI

@main
struct AccessibilityApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
