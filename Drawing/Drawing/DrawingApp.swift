//
//  DrawingApp.swift
//  Drawing
//
//  Created by Vojta Molda on 7/3/20.
//

import SwiftUI

@main
struct DrawingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
