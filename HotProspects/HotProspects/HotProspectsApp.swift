//
//  HotProspectsApp.swift
//  HotProspects
//
//  Created by Vojta Molda on 7/30/20.
//

import SwiftUI

@main
struct HotProspectsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
