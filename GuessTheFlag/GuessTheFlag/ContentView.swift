//
//  ContentView.swift
//  GuessTheFlag
//
//  Created by Vojta Molda on 7/2/20.
//

import SwiftUI

struct ContentView: View {
    @State private var countries = [
        "Estonia", "France", "Germany", "Ireland", "Italy",
        "Nigeria", "Poland", "Russia", "Spain", "UK", "US"
        ].shuffled()
    
    private let labels = [
        "Estonia": "Flag with three horizontal stripes. Top stripe blue, middle white and bottom stripe black",
        "France": "Flag with three vertical stripes. Left stripe blue, middle stripe white and right stripe red",
        "Germany": "Flag with three horizontal stripes. Top stripe black, middle stripe red and bottom stripe yellow",
        "Ireland": "Flag with three vertical stripes. Left stripe green, middle stripe white and right stripe orange",
        "Italy": "Flag with three vertical stripes. Left stripe green, middle stripe white and right stripe red",
        "Nigeria": "Flag with three vertical stripes. Left stripe and right stripes green, middle stripe white",
        "Poland": "Flag with two horizontal stripes. Top stripe white and bottom stripe red",
        "Russia": "Flag with three horizontal stripes. Top stripe white, middle stripe blue and bottom stripe red",
        "Spain": "Three horizontal stripes top and bottom red, and middle yellow stripe twice the height of the other ones.",
        "UK": "Centered red cross bordered in white with a diagonal white cross with red saltire placed on blue field",
        "US": "Thirteen equal horizontal stripes of red alternating with white with a blue rectangle in the top left corner painted with 50 stars",
    ]
    
    @State private var correctAnswer = Int.random(in: 0...2)
    @State private var showingScore = false
    @State private var scoreTitle = ""

    var body: some View {
        ZStack {
            LinearGradient(
                gradient: Gradient(colors: [.blue, .black]),
                startPoint: .top, endPoint: .bottom
            ).edgesIgnoringSafeArea(.all)

            VStack(spacing: 30) {
                VStack {
                    Text("Tap the flag...").foregroundColor(.white)
                    Text(countries[correctAnswer])
                        .foregroundColor(.white)
                        .font(.largeTitle)
                        .fontWeight(.black)
                }
                ForEach(0 ..< 3) { number in
                    Button(action: {
                        self.flagTapped(number)
                        // TODO: Flag was tapped
                    }) {
                        Image(self.countries[number])
                            .renderingMode(.original)
                            .clipShape(Capsule())
                            .overlay(Capsule().stroke(Color.black, lineWidth: 1)
                            .shadow(color: .black, radius: 2)
                            .accessibility(label: Text(verbatim: self.labels[self.countries[number],
                                                       default: "Unknown flag"])
                            )
                        )
                    }
                }
                Spacer()
            }
            .alert(isPresented: $showingScore) {
                Alert(title: Text(scoreTitle),
                      message: Text("Your score is ???"),
                      dismissButton: .default(Text("Continue")) {
                        self.askQuestion()
                    }
                )
            }
        }
    }
    
    func flagTapped(_ number: Int) {
        if number == correctAnswer {
            scoreTitle = "Correct"
        } else {
            scoreTitle = "Wrong"
        }
        showingScore = true
    }
    
    func askQuestion() {
        countries.shuffle()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
