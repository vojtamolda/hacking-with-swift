//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by Vojta Molda on 7/2/20.
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
